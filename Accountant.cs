﻿using System;

namespace OTUS_HW10_Prototype
{
    class Accountant : Specialist
    {
        private readonly bool _canCountMoney;
        public Accountant(bool canCountMoney, string name, short age, string gender) : base("Accountant", name, age, gender)
        {
            _canCountMoney = canCountMoney;
        }

        public override Human Copy()
        {
            return new Accountant(_canCountMoney, _name, _age, _gender);
        }
        public override object Clone()
        {
            return Copy();
        }

        public void CountMoney()
        {
            Console.WriteLine("Counting money...");
        }

        public override void FuckUp()
        {
            PressSomeKey();
        }

        public void PressSomeKey()
        {
            Console.WriteLine($"{_name} Pressed some key and everything got broken");
        }

        public override string ToString()
        {
            return $"Can count money: {_canCountMoney} " + base.ToString();
        }

    }
}
