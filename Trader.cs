﻿using System;


namespace OTUS_HW10_Prototype
{
    class Trader : Specialist
    {
        private readonly bool _canMakeMoney;
        public Trader(bool canMakeMoney, string name, short age, string gender) : base("Trader", name, age, gender)
        {
            _canMakeMoney = canMakeMoney;
        }

        public override Human Copy()
        {
            return new Trader(_canMakeMoney, _name, _age, _gender);
        }
        public override object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return $"Can make money: {_canMakeMoney} " + base.ToString();
        }

        public override void FuckUp()
        {
            BuyOnHigh();
        }

        public void BuyOnHigh()
        {
            Console.WriteLine($"{_name} bought on high");
        }
    }
}
