﻿using System;

namespace OTUS_HW10_Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            TestHumans test = new TestHumans();
            test.TestProgrammers();
            test.TestTraders();
            test.TestAccountants();
            Console.ReadKey();
        }
    }
}
