﻿using System;


namespace OTUS_HW10_Prototype
{
    class TestHumans
    {
        public void TestProgrammers()
        {
            Programmer vasya = new Programmer(true, "Vasya", 22, "M");
            Programmer petya = new Programmer(false, "Petya", 20, "M");
            Programmer masha = new Programmer(true, "Masha", 19, "F");

            Programmer vasya1 = (Programmer)vasya.Copy();
            Programmer petya1 = (Programmer)petya.Copy();
            Programmer masha1 = (Programmer)masha.Copy();

            Programmer vasya2 = (Programmer)vasya1.Clone();
            Programmer petya2 = (Programmer)petya1.Clone();
            Programmer masha2 = (Programmer)masha1.Clone();

            Console.WriteLine("These programmers we got: ");
            Console.WriteLine("0: " + vasya.ToString());
            Console.WriteLine("1: " + vasya1.ToString());
            Console.WriteLine("2: " + vasya2.ToString());

            Console.WriteLine("0: " + petya.ToString());
            Console.WriteLine("1: " + petya1.ToString());
            Console.WriteLine("2: " + petya2.ToString());

            Console.WriteLine("0: " + masha.ToString());
            Console.WriteLine("1: " + masha1.ToString());
            Console.WriteLine("2: " + masha2.ToString());

            vasya.DoWork();
            vasya1.FuckUp();
                        
        }

        public void TestTraders()
        {
            Trader alex = new Trader(true, "Alex", 22, "M");
            Trader sasha = new Trader(false, "Sasha", 26, "M");
            Trader lena = new Trader(true, "Lena", 21, "F");

            Trader alex1 = (Trader)alex.Copy();
            Trader sasha1 = (Trader)sasha.Copy();
            Trader lena1 = (Trader)lena.Copy();

            Trader alex2 = (Trader)alex1.Clone();
            Trader sasha2 = (Trader)sasha1.Clone();
            Trader lena2 = (Trader)lena1.Clone();

            Console.WriteLine("These traders we got: ");
            Console.WriteLine("0: " + alex.ToString());
            Console.WriteLine("1: " + alex1.ToString());
            Console.WriteLine("2: " + alex2.ToString());

            Console.WriteLine("0: " + sasha.ToString());
            Console.WriteLine("1: " + sasha1.ToString());
            Console.WriteLine("2: " + sasha2.ToString());

            Console.WriteLine("0: " + lena.ToString());
            Console.WriteLine("1: " + lena1.ToString());
            Console.WriteLine("2: " + lena2.ToString());

            alex.DoWork();
            alex1.FuckUp();

        }

        public void TestAccountants()
        {
            Accountant kolya = new Accountant(true, "Kolya", 24, "M");
            Accountant fedor = new Accountant(false, "Dyadya Fedor", 46, "M");
            Accountant vera = new Accountant(true, "Vera Ivanovna", 23, "F");

            Accountant kolya1 = (Accountant)kolya.Copy();
            Accountant fedor1 = (Accountant)fedor.Copy();
            Accountant vera1 = (Accountant)vera.Copy();

            Accountant kolya2 = (Accountant)kolya1.Clone();
            Accountant fedor2 = (Accountant)fedor1.Clone();
            Accountant vera2 = (Accountant)vera1.Clone();

            Console.WriteLine("These Accountants we got: ");
            Console.WriteLine("0: " + kolya.ToString());
            Console.WriteLine("1: " + kolya1.ToString());
            Console.WriteLine("2: " + kolya2.ToString());

            Console.WriteLine("0: " + fedor.ToString());
            Console.WriteLine("1: " + fedor1.ToString());
            Console.WriteLine("2: " + fedor2.ToString());

            Console.WriteLine("0: " + vera.ToString());
            Console.WriteLine("1: " + vera1.ToString());
            Console.WriteLine("2: " + vera2.ToString());

            kolya.DoWork();
            kolya1.FuckUp();
            
        }
    }
}
