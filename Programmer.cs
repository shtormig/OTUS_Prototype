﻿using System;


namespace OTUS_HW10_Prototype
{
    class Programmer : Specialist
    {
        private readonly bool _canWriteCode;
        public Programmer(bool canWriteCode, string name, short age, string gender) : base("Programmer", name, age, gender)
        {
            _canWriteCode = canWriteCode;
        }

        public void WriteCode()
        {
            Console.WriteLine("Write code");
        }

        public override Human Copy()
        {
            return new Programmer(_canWriteCode, _name, _age, _gender);
        }
        public override object Clone()
        {
            return Copy();
        }

        public override string ToString()
        {
            return $"Can write code: {_canWriteCode} "+base.ToString();
        }
    }       
}
