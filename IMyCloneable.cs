﻿namespace OTUS_HW10_Prototype
{
    interface IMyCloneable <T>
    {
        T Copy();
    }
}
